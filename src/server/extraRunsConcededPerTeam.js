/* Extra runs conceded per team in the year 2016  */

const csvToJson = require("csvtojson");
const fs = require("fs");

function getExtraRunConcededPerTeam() {
    csvToJson()
        .fromFile("../data/matches.csv")
        .then((matchInfoObj) => {
            csvToJson()
                .fromFile("../data/deliveries.csv")
                .then((deliveriesInfoObj) => {
                    const result = {};
                    deliveriesInfoObj.forEach((i) => {
                        if (matchInfoObj.find((j) => { if (j.season == 2016 && i.match_id == j.id) { return true } })) {
                            if (!(i.bowling_team in result)) {
                                result[i.bowling_team] = parseInt(i.extra_runs);
                            } else {
                                result[i.bowling_team] += parseInt(i.extra_runs);
                            }
                        }
                    })
                    fs.writeFileSync("../public/output/extraRunConcededPerTeam.json", JSON.stringify(result), "utf-8");
                })

        })
}
getExtraRunConcededPerTeam();