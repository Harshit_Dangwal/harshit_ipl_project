Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Matches Per Year'
    },
    xAxis: {
        title: {
            text: 'Teams'
        },
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Matches'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Total matches played'
    },
    series: [{
        name: 'Population',
        data: [
            ['2008', 58],
            ['2009', 57],
            ['2010', 60],
            ['2011', 73],
            ['2012', 74],
            ['2013', 76],
            ['2014', 60],
            ['2015', 59],
            ['2016', 60],
            ['2017', 59],
        ],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});