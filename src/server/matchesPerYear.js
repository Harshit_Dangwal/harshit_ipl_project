/* Number of matches played per year for all the years in IPL */

const csvToJson = require("csvtojson");
const fs = require("fs")

function getMatchesPerYear() {
    csvToJson()
        .fromFile("../data/matches.csv")
        .then(matchInfo => {
            result = {};
            matchInfo.forEach((obj)=>{
                if (obj.season in result){
                    result[obj.season]+=1;
                }else{
                    result[obj.season]=1;
                }
            })
            fs.writeFileSync("../public/output/matchesPerYear.json", JSON.stringify(result), "utf-8");
        },
            (error) => {
                console.log(error)
            }
        )
}

getMatchesPerYear();
