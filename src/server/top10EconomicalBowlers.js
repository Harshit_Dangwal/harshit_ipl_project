/* Top 10 economical bowlers in the year 2015 */

const csvToJson = require("csvtojson");
const fs = require("fs");
const { resourceLimits } = require("worker_threads");

function getTop10EconomicalBowlers() {
    csvToJson()
        .fromFile("../data/matches.csv")
        .then((matchObj) => {
            csvToJson()
                .fromFile("../data/deliveries.csv")
                .then((deliveriesObj) => {
                    const tempObj = {};
                    const result = [];
                    deliveriesObj.forEach((i) => {
                        if (matchObj.find((j) => { if (j.season == 2015 && i.match_id == j.id) { return true } })) {
                            if (!(i.bowler in tempObj)) {
                                tempObj[i.bowler] = { ["total runs"]: parseInt(i.total_runs), ['total balls']: 1 };
                            } else {
                                tempObj[i.bowler]['total runs'] += parseInt(i.total_runs);
                                tempObj[i.bowler]['total balls'] += 1;
                            }
                        }
                    })
                    const compareFunction = (a, b) => {
                        return a - b;
                    }
                    let temp = {}
                    for (i in tempObj) {
                        temp[i] = tempObj[i]['total runs'] * 6 / tempObj[i]['total balls'];
                    }
                    let sortedBowlers = Object.values(temp).sort(compareFunction)
                    for (i of sortedBowlers) {
                        for (j in temp) {
                            if (i == temp[j]) {
                                result.push(j);
                            }
                        }
                        if (result.length == 10) {
                            break;
                        }
                    }
                    console.log(result)
                    fs.writeFileSync("../public/output/top10EconomicalBowlers.json", JSON.stringify(result), "utf-8");

                })
        })
}
getTop10EconomicalBowlers()