/* Number of matches won per team per year in IPL. */

const csvToJason = require("csvtojson");
const fs = require("fs");

function getMatchesWonPerTeam() {
    csvToJason()
        .fromFile("../data/matches.csv")
        .then((matchObjInfo) => {
            const result = {};
            matchObjInfo.forEach((i) => {
                if (!(i.winner in result)) {
                    result[i.winner] = { [i.season]: 1 };
                } else {
                    if (i.winner in result && i.season in result[i.winner]) {
                        result[i.winner][i.season] += 1;
                    } else {
                        result[i.winner][i.season] = 1
                    }
                }
            })
            fs.writeFileSync("../public/output/teamWonPerYear.json", JSON.stringify(result), "utf-8");
            (error)=>{
                console.log(error);
            }

        })
}

getMatchesWonPerTeam();